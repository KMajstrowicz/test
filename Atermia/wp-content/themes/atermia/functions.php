<?php
/**
 * Wordpress template created for "Atermia"
 *
 * Version 1.0
 * Date: 04.10.2019
 *
 * @author INTR
 * @package WordPress
 *
 */


if(!defined('THEME_DIR')) {
    define('THEME_DIR', get_theme_root(). '/' .get_template() . '/');
}

if(!defined('THEME_URL')) {
    define('THEME_URL', WP_CONTENT_URL. '/themes/' .get_template() . '/');
}


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Ustawienia aplikacji',
		'menu_title' 	=> 'Ustawienia aplikacji',
		'redirect' 		=> false
	));
}

// webpack
function wp_enqueue() {
    wp_enqueue_style('main_css', get_template_directory_uri() . '/assets/styles/main.css', array(), '1.0', false);
    wp_enqueue_script('main_js', get_template_directory_uri() . '/assets/scripts/main.js', array(), '1.0', true);
}
add_action('wp_enqueue_scripts', 'wp_enqueue');

//register_nav_menus(array(
//    'menu_main' => 'Menu main',
//    'menu_footer_categories' => 'Footer menu categories',
//    'menu_footer_main' => 'Footer menu main',
//    'menu_footer_brands' => 'Footer menu brands',
//));